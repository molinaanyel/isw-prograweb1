<?php
  
  $connection = new mysqli('localhost:3306', 'root', 'Anyel123', 'categorias');
  $sentencia = $connection -> query("select *from cate");
  $category = $sentencia->fetch_all();
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<title>Document</title>
</head>
<body>
<div class="container">
  <?php require ('header.php') ?>
  <h1>List of Users</h1>
    <table class="table table-light">
      <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Actions</th>
      </tr>
      <tbody>
        <?php
          // loop users
          foreach($category as $cate) {
            echo "<tr><td>".$cate[1]."</td>
            <td>".$cate[2]."</td>
            <td><i><a href=\"edit.php?id=".$cate[0]."\">Edit</a></i>
            /<i> <a onclick=\"return confirm('Estas seguro de eliminar?');\" href=\"delete.php?id=".$cate[0]."\">Delete  </a> </i>
            </td></tr>";
           
          }
        ?>
      </tbody>
    </table>
    <?php

  $connection->close();
?>
</div>
</body>
</html>
