<?php
  require ('funciones.php');
  $logica = new Metodo();
  $matriculas= $logica->getMatriculaPorCarrera();
  
?>




<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CRUD con PHP usando Programación Orientada a Objetos</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/custom.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
</head>
<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Listado de  <b>Matriculas</b></h2></div>
                    <div class="col-sm-4">
                        <a href="crear.php" class="btn btn-info add-new"><i class="fa fa-plus"></i> Matricula</a>
                    </div>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Correo</th>
			            <th>Carrera</th>
                    </tr>
                </thead>
                 
                <tbody> 
                <?php
                        // loop users
                        foreach($matriculas as $m) {
                            echo "<tr><td>".$m[0]."</td>
                            <td>".$m[1]."</td>
                            <td>".$m[2]."</td>
                            <td>".$m[3]."</td>
                                               ";
                        
                        }
                        ?>   
                          
                </tbody>
            </table>
        </div>
    </div>     
</body>
</html>
