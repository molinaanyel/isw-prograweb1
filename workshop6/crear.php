
<?php
  require ('funciones.php');
  $logica = new Metodo();
  $carreras= $logica->getCarreras();
  
?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CRUD con PHP usando Programación Orientada a Objetos</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/custom.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
</head>
<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Agregar <b>Matricula</b></h2></div>
                   
                </div>
            </div>
			<div class="row">
				<form  action="guardar.php" method="POST" >
				<div class="col-md-6">
					<label>Nombre</label>
					<input type="text" name="nombre" id="nombres" class='form-control' maxlength="100" required >
				</div>
				<div class="col-md-6 mt-3">
					<label>Apellido:</label>
					<input type="text" name="apellido" id="apellidos" class='form-control' maxlength="100" required>
				</div>
				<div class="col-md-12">
					<label>Correo electrónico:</label>
					<input  name="correo" id="direccion" class='form-control' maxlength="255" required >
				</div>
				<div class="col-md-6">
					<label>Cédula:</label>
					<input type="text" name="cedula" id="cedula" class='form-control' maxlength="15" required >
				</div>
				<div class="col-md-6">
				<label>Cédula:</label>

					<select  class='form-control' name='carrera'>

				
                        <?php
							
                            foreach($carreras as  $ca) {
                            echo "<option value=\"$ca[0]\">$ca[1]</option>";
                            }
                        ?>
                           
                        </select>
				
				</div>
				
				<div class="col-md-12 pull-right">
				<hr>
					<button type="submit" class="btn btn-success">Guardar datos</button>
				</div>
				</form>
			</div>
        </div>
    </div>     
</body>
</html>